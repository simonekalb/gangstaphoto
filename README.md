# README #

GangstaPhoto is a simple app that shows all your Instagram(tm) photos and browse it in a simple yet powerful way.


### What is this repository for? ###

* Quick summary
This repo holds the source code for GangstaPhoto app.

* Version
v 0.1


### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin

[GPL v 3.0](https://www.gnu.org/licenses/gpl-3.0.en.html) Licence 
[Simone Kalb](https://twitter.com/simonekalb)


* Other community or team contact